//
//  SumViewController.swift
//  CombineTest
//
//  Created by Tom Eestermans on 05/02/2021.
//

import Combine
import UIKit

/*
 After our first experience with Combine in the TextReflectionViewController, we're trying something new here.
 
 What I want to achieve in this ViewController is automatically summing the decimal values typed in two TextFields, showing the result in a label.
 What I want to demonstrate as well, is the importance and benefit of the .eraseToAnyPublisher function
 
 Again, I've set up a publisher for when the text in a UITextField changes. Two this time, because we have two textfields.
 The new combine function I want to work with here is combineLatest.
 combineLatest receives and combines the latest elements from 2 (,3 or 4) publishers.
 Using combineLatest, I want to combine the latest values from the textfields, sum them and show them on a label.
 
 A real world scenario where you'd want to use this is for example in credential validations, and dis/enabling a button in response.
 Say you have a publisher for the result of username validation, and a publisher for the result of password validation.
 You can set your code up with combineLatest to only enable the 'sign up' or 'log in' button, when both are valid.
 The nice thing about this, is that it's a reactive chain.
 When one of the validation states changes, it immediately fires the chain, responding to the latest states and enabling your button correctly.
 
 Lets try out combineLatest.
 */

class SumViewController: UIViewController {
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    var cancellables: Set<AnyCancellable> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create the subscribers for textfield input as a double for the two textfields
        let firstPublisher: AnyPublisher<Double, Never> = self.createNumberPublisherFor(textField: self.firstTextField)
            .print("First textfield")
            .eraseToAnyPublisher()
        
        let secondPublisher: AnyPublisher<Double, Never> = self.createNumberPublisherFor(textField: self.secondTextField)
            .print("Second textfield")
            .eraseToAnyPublisher()
        
        // Create a combineLatest publisher.
        // This publisher will only pass through once it has received values from both publishers you want to combine.
        // After this, it will pass through every time one of the publishers has a new value.
        let newValuesPublisher: Publishers.CombineLatest<AnyPublisher<Double, Never>, AnyPublisher<Double, Never>> = firstPublisher.combineLatest(secondPublisher)
        
        // Create a publisher that sums the two input values
        let unreadableResultPublisher: Publishers.Map<Publishers.CombineLatest<AnyPublisher<Double, Never>, AnyPublisher<Double, Never>>, Double> = newValuesPublisher.map { (newValues: (Double, Double)) -> Double in
            return newValues.0 + newValues.1
        }
        
        // Barely readable, the type for those publishers, right? That's why we use eraseToAnyPublisher when we want to return a publisher from a function.
        let resultPublisher: AnyPublisher<Double, Never> = newValuesPublisher.map { (newValues: (Double, Double)) -> Double in
            return newValues.0 + newValues.1
        }.eraseToAnyPublisher()
        
        // Map the result to a string and assign it to the label.
        // Because this is not the final version I want to use, I'll sink and print it instead of assign it to the label
        resultPublisher
            .map({ (result: Double) -> String? in
                return String(result)
            })
            .sink(receiveValue: { (stringResult: String?) in
                print("Result of the long variant: \(stringResult ?? "nil")")
            })
            .store(in: &self.cancellables)
        
        // When you dont want to return and don't necessarily care for storing each publisher individually in a variable, you can chain all these calls
        // This will look like this, this code does exactly the same as the code above:
        firstPublisher.combineLatest(secondPublisher)
            .map { (newValues: (Double, Double)) -> Double in
                return newValues.0 + newValues.1
            }
            .map({ (result: Double) -> String? in
                return String(result)
            })
            .sink(receiveValue: { (stringResult: String?) in
                print("Result of the chained variant: \(stringResult ?? "nil")")
            })
            .store(in: &self.cancellables)
        
        // Doing this you can see that you can actually optimise this, combining the map calls
        // Because this is the chain I actually want to use, I'll assign instead of sink this one
        firstPublisher.combineLatest(secondPublisher)
            .map { (newValues: (Double, Double)) -> String? in
                let result: Double = newValues.0 + newValues.1
                return String(result)
            }
            .assign(to: \.text, on: self.resultLabel)
            .store(in: &self.cancellables)
    }
    
    /// Creates a publisher for changes in the text in the given textfield and returns it as a Double publisher.
    /// - Parameter textField: The textfield to create a publisher for
    /// - Returns: A publisher for the text value as a Double in the given textfield
    private func createNumberPublisherFor(textField: UITextField) -> AnyPublisher<Double, Never> {
        return NotificationCenter.default
            // Create a publisher for the textDidChangeNotification on our textfield
            .publisher(for: UITextField.textDidChangeNotification,
                       object: textField)
            // When we receive the notification, map the notification publisher to a Double publisher for the actual text
            .map { (notification: Notification) -> Double in
                guard
                    let text: String = (notification.object as? UITextField)?.text,
                    let input: Double = Double(text)
                else {
                    return 0
                }
                
                return input
            }
            // "hide" our type of our final publisher
            // This makes it readable and makes it easier to build on it on further calls.
            // Also, the user of this function is not interested in our way of creating the publisher, it just wants a publisher. This way we can hide our method and easyly make changes if needed, as long as we always end up with our expected return value of AnyPublisher<String?, Never>
            .eraseToAnyPublisher()
    }
}
