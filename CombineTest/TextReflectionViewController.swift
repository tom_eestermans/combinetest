//
//  ViewController.swift
//  CombineTest
//
//  Created by Tom Eestermans on 03/02/2021.
//

import Combine
import UIKit

//TODO: Maak intro pager die d.m.v. Combine bekijkt of je alle pagina's gezien hebt!

/*
 This UIViewController is my first attempt at and experience with Combine.
 
 I've tried to set up a publisher for when the text in my UITextField changes.
 On this publisher, I ended up adding two subscribers.
 
 
 The first one was to test out if my publisher and subscriber actually work.
 It immediately reflects the changes in the textfield and assigns them to the resultlabel.
 
 The second one was to test out more of a real world situation, where you'd like the subscriber only to fire after a small delay.
 The real world application I had in mind was a text input that has to be validated via a network call.
 A username validation for instance, where you'd like to verify that the username doesn't already exist.
 An other application could be a search query.
 Both of these calls would be spammed when you'd fire them for every change in the textfield.
 With the debounce I've added, the last received value is held until an inactivity period of the given value has expired.
 */

class TextReflectionViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var throttledResultLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var cancellables: Set<AnyCancellable> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create a publisher for the text on our textfield
        let textPublisher: AnyPublisher<String?, Never> = self.createTextPublisherFor(textField: self.textField)
        
        // The first subscriber (an assign subscriber) will assign the received String? directly to the text property of the resultLabel
        // We have to store a reference of subscribers to prevent them from being deallocated.
        textPublisher
            .assign(to: \.text, on: self.resultLabel)
            .store(in: &cancellables)
        
        // The second subscriber (again assign, but on the throttledResultLabel) has a debounce.
        // Debounce will only pass through the latest received value after an inactivity in receiving values of more than the given time.
        textPublisher
            .debounce(for: .seconds(0.5),
                      scheduler: RunLoop.main)
            .assign(to: \.text, on: self.throttledResultLabel)
            .store(in: &cancellables)
    }
    
    /// Creates a publisher for changes in the text in the given textfield.
    /// - Parameter textField: The textfield to create a publisher for
    /// - Returns: A publisher for the text value in the given textfield
    private func createTextPublisherFor(textField: UITextField) -> AnyPublisher<String?, Never> {
        return NotificationCenter.default
            // Create a publisher for the textDidChangeNotification on our textfield
            .publisher(for: UITextField.textDidChangeNotification,
                       object: textField)
            // When we receive the notification, map the notification publisher to a text publisher for the actual text, or a space
            .map { (notification: Notification) -> String in
                guard let text = (notification.object as? UITextField)?.text else {
                    return " "
                }
                
                if text == "" {
                    return " "
                }
                
                return text
            }
            // Set up the text publisher to accept multiple subscribers. This makes sure all subscribers receive the same value.
            // Not particularly necessary in this case, but would be if you'd publish random values for instance.
            // If you'd like all receivers to receive the same random values, a share is required.
            .share()
            // "hide" our type of our final publisher, which is actually the following now:
            // AnyPublisher<String?, Publishers.Map<NotificationCenter.Publisher, String?>.Failure>
            // This makes it readable and makes it easier to build on it on further calls.
            // Also, the user of this function is not interested in our way of creating the publisher, it just wants a publisher. This way we can hide our method and easyly make changes if needed, as long as we always end up with our expected return value of AnyPublisher<String?, Never>
            .eraseToAnyPublisher()
    }
}

