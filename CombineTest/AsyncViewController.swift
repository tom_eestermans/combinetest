//
//  AsyncViewController.swift
//  CombineTest
//
//  Created by Tom Eestermans on 09/02/2021.
//

import Combine
import UIKit

/*
 Alright, after our first experience with Combine in the TextReflectionViewController and our second test in the SumViewController, lets try something more advanced
 
 Where I think Combine can really shine is when async operations are involved.
 The ability to Combine (ha, where would they've come up with the name) results from multiple (a)sync processes can be really powerful.
 
 For this tryout, I've found a free to use JSON API at https://jsonplaceholder.typicode.com/
 This API has, among other things, an endpoint to retrieve Albums, and an endpoint to retrieve Photos.
 As you might have guessed, photo's belong to albums.
 I've created a model for Album and Photo corresponding to their JSON format. In Album, I've added an additional array of photos.
 We're going to retrieve the albums and the photos at the same time.
 When both are ready, we'll combine the results and fill our album models with their respective photos.
 
 In this case, I'm combining two asynchronous operations. This can also be done mixing it up with synchronous operations etc.
 */

class AsyncViewController: UIViewController {
    @IBOutlet weak var albumsAmountLabel: UILabel!
    @IBOutlet weak var albumsActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var albumsStatusImage: UIImageView!
    
    @IBOutlet weak var photosAmountLabel: UILabel!
    @IBOutlet weak var photosActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var photosStatusImage: UIImageView!
    
    @IBOutlet weak var albumsAssembledAmountLabel: UILabel!
    @IBOutlet weak var albumsAssembledActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var albumsAssembledCheckMarkImage: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    
    let failedImage: UIImage = UIImage(systemName: "xmark.circle")!
    let successImage: UIImage = UIImage(systemName: "checkmark.circle")!
    
    let baseUrl: URL = {
        guard let url: URL = URL(string: "https://jsonplaceholder.typicode.com/") else {
            fatalError("Unable to create URL")
        }
        
        return url
    }()
    
    var cancellables: [AnyCancellable] = []
    
    @Published var albums: [Album] = []
    @Published var photos: [Photo] = []
    
    @Published var albumsAssembled: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // In the viewDidLoad, we set up our subscribers that we want to have in place during the lifecycle of our viewcontroller
        
        // Create a subscription on the subscriber for our albums property, which shows the amount of albums in a label
        $albums
            .receive(on: RunLoop.main)
            .sink { (albums: [Album]) in
                self.albumsAmountLabel.text = String(albums.count)
            }
            .store(in: &self.cancellables)
        
        // Create a subscription on the subscriber for our photos property, which shows the amount of albums in a label
        $photos
            .receive(on: RunLoop.main)
            .sink { (photos: [Photo]) in
                self.photosAmountLabel.text = String(photos.count)
            }
            .store(in: &self.cancellables)
        
        // Here, we create a subscription on our integer containing the amount of assembled albums
        // Using debounce, as we've learned in the TextReflectionViewController, we can make sure
        // this chain is not being spammed the entire time.
        // We are mapping the result to a Tuple<Bool, String>, indicating wether we're ready and passing on the current counter as a string.
        // We set our amount label to the text from the map and hide our checkmark image according to our ready status.
        // When we're ready, we stop the activity indicator, reflect success on the statuslabel and reload the tableview with our albums
        $albumsAssembled
            .debounce(for: .seconds(0.1), scheduler: RunLoop.main)
            .map { (assembled: Int) -> (Bool, String) in
                return (self.albumsAssembled == self.albums.count, "\(assembled)/\(self.albums.count)")
            }
            .sink(receiveValue: { (assembleStatus: (Bool, String)) in
                // If we're ready
                if (assembleStatus.0) {
                    self.albumsAssembledActivityIndicator.stopAnimating()
                    self.reflectSuccessOn(imageView: self.albumsAssembledCheckMarkImage)
                    self.tableView.reloadData()
                }
                
                self.albumsAssembledCheckMarkImage.isHidden = !assembleStatus.0
                self.albumsAssembledAmountLabel.text = assembleStatus.1
            })
            .store(in: &self.cancellables)
        
        // When there is a new value for either photo's or albums, we want to start combining the datasets to fil our album models with photo's
        $albums.combineLatest($photos)
            .sink { (combined: (Published<[Album]>.Publisher.Output, Published<[Photo]>.Publisher.Output)) in
                self.albumsAssembledActivityIndicator.startAnimating()
                self.albumsAssembled = 0
                
                let albums: [Album] = combined.0
                let photos: [Photo] = combined.1
                
                // Calling publisher on an array publishes every single value separately. It's like a foreach but I think it's async?
                // Not sure though. Did it anyway in the light of this being a combine tryout.
                albums.publisher
                    .sink { (album: Album) in
                        album.photos =  photos.filter { (photo: Photo) -> Bool in
                            return photo.albumId == album.id
                        }
                        self.albumsAssembled += 1
                    }
                    .store(in: &self.cancellables)
            }
            .store(in: &self.cancellables)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Retrieve albums and photo's on view appearance
        getAlbums()
        getPhotos()
    }
    
    func getAlbums() {
        self.albumsStatusImage.isHidden = true
        self.albumsActivityIndicator.startAnimating()
        
        // Create a dataTaskPublisher, decode the result to an array of albums
        // Store it in our property and reflect the status in the UI.
        URLSession.shared.dataTaskPublisher(for: baseUrl.appendingPathComponent("albums"))
            .tryMap { (result: (data: Data, response: URLResponse)) -> Data in
                return result.data
            }
            .decode(type: [Album].self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .sink { (completion: Subscribers.Completion<Error>) in
                switch completion {
                case .failure(let error):
                    print("Getting albums failed: \(error)")
                    self.reflectFailureOn(imageView: self.albumsStatusImage)
                    break
                default:
                    self.reflectSuccessOn(imageView: self.albumsStatusImage)
                    break
                }
                
                self.albumsActivityIndicator.stopAnimating()
                self.albumsStatusImage.isHidden = false
            }
            receiveValue: { (albums: [Album]) in
                self.albums = albums
            }
            .store(in: &self.cancellables)
    }
    
    func getPhotos() {        
        self.photosStatusImage.isHidden = true
        self.photosActivityIndicator.startAnimating()
        
        // Create a dataTaskPublisher, decode the result to an array of photos
        // Store it in our property and reflect the status in the UI.
        URLSession.shared.dataTaskPublisher(for: baseUrl.appendingPathComponent("photos"))
            .tryMap { (result: (data: Data, response: URLResponse)) -> Data in
                return result.data
            }
            .decode(type: [Photo].self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .sink { (completion: Subscribers.Completion<Error>) in
                switch completion {
                case .failure(let error):
                    print("Getting photos failed: \(error)")
                    self.reflectFailureOn(imageView: self.photosStatusImage)
                    break
                default:
                    self.reflectSuccessOn(imageView: self.photosStatusImage)
                    break
                }
                
                self.photosActivityIndicator.stopAnimating()
                self.photosStatusImage.isHidden = false
            } receiveValue: { (photos: [Photo]) in
                self.photos = photos
            }
            .store(in: &self.cancellables)
    }
    
    func reflectSuccessOn(imageView: UIImageView) {
        imageView.image = self.successImage
        imageView.tintColor = UIColor.systemGreen
    }
    
    func reflectFailureOn(imageView: UIImageView) {
        imageView.image = self.failedImage
        imageView.tintColor = UIColor.systemRed
    }
    
    @IBAction func downloadAlbumsButtonPressed(_ sender: Any) {
        getAlbums()
    }
    
    @IBAction func clearAlbumsButtonPressed(_ sender: Any) {
        self.albums.removeAll()
    }
    
    @IBAction func downloadPhotosButtonPressed(_ sender: Any) {
        getPhotos()
    }
    
    @IBAction func clearPhotosButtonPressed(_ sender: Any) {
        self.photos.removeAll()
    }
}

extension AsyncViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Albums"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath)
        
        guard
            let album: Album = (self.albums.first { (album: Album) -> Bool in
                return album.id == (indexPath.row + 1)
            })
        else {
            return cell
        }
        
        cell.textLabel?.text = album.title
        cell.detailTextLabel?.text = "Photos: \(album.photos.count)"
        
        return cell
    }
}

class Album: Decodable {
    let userId: Int
    let id: Int
    let title: String
    
    var photos: [Photo] = []
    
    enum CodingKeys: String, CodingKey {
        case userId
        case id
        case title
    }
}

class Photo: Decodable {
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
}
