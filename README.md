# Combine #



In this project I've made my first effort in getting familiar with Apples Combine framework. To read more about Combine, here is the [Apple Developer page](https://developer.apple.com/documentation/combine)

This project is written in Swift and the focus was on using Combine. You can run it in the simulator to try it out for yourself.

### How is the project structured? ###

I have created three viewcontrollers, corresponding to the three scenario's I wanted to try out.

* TextReflectionViewController
* SumViewController
* AsyncViewController

The viewcontrollers are accessible via a tabbar at the bottom, labeled 1, 2 and 3. Read below about the scenarios 

### TextReflectionViewController ###

In this viewcontroller I wanted to simulate the real world scenario where you'd like to give the user a responsive UI on typed input. For example, username validation via a server, or giving live search results.

In these cases, you don't want to fire a request every time a change happens in the textview, you'd be spamming the server. Using Combines [debounce](https://developer.apple.com/documentation/combine/fail/debounce(for:scheduler:options:)) operator, you can limit the amount of requests until a set inactivity period has completed. 

### SumViewController ###

In this viewcontroller I wanted to experiment with Combines [combineLatest](https://developer.apple.com/documentation/combine/fail/combinelatest(_:_:)-80pmk) operator. This operator enables you to combines the latest values from multiple publishers and handle their output as a Tuple further on in your combine chain.

The example I use is to automatically sum the decimal input from two textfields and output the result on a label.

### AsyncViewController ###

In this viewcontroller I wanted to try out something more advanced. A scenario where I think Combine can really shine: combining (ha, where would they have found the framworks name) multiple (a)sync operations.

For this tryout, I've found and use a [free to use JSON API](https://jsonplaceholder.typicode.com/)
This API has, among other things, an endpoint to retrieve Albums, and an endpoint to retrieve Photos. As you might have guessed, photo's belong to albums. I've created a model for Album and Photo corresponding to their JSON format. In Album, I've added an additional array of photos.

We're going to retrieve the albums and the photos at the same time. When both are ready, we'll combine the results and fill our album models with their respective photos.

In this case, I'm combining two asynchronous operations. This can also be done mixing it up with synchronous operations etc.

***

I hope you've learned something here. I did, but I'm not yet fully convinced why I should use Combine over regular blocks/closures on single processes. I am only really seeing the benefit when you start combining multiple (a)synchronous operations. All in all it was an interesting experiment and I still have some ideas on how to expand this project using Combine. Let me know your thoughts!

Tom Eestermans

_tom.eestermans@fourtress.nl_
